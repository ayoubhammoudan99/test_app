import 'dart:async';

class CounterController {
  CounterController() {
    addCounter.add(0);
  }
  StreamController<int> _conuterController = StreamController<int>();
  Stream<int> get counterStream => _conuterController.stream;
  Sink<int> get addCounter => _conuterController.sink;
}
