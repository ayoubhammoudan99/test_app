import 'dart:async';

import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import 'counter_controller.dart';

void main() {
  runApp(TestApp());
}

class TestApp extends StatefulWidget {
  TestApp({super.key});

  @override
  State<TestApp> createState() => _TestAppState();
}

class _TestAppState extends State<TestApp> {
  int _counter = 0;
  String connectionType = "";
  @override
  void initState() {
    // check();

    // Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
    //   setState(() {
    //     connectionType = result.name;
    //   });
    // });

    super.initState();
  }

  Future check() async {
    final ConnectivityResult connectivityResult =
        await (Connectivity().checkConnectivity());

    setState(() {
      connectionType = connectivityResult.name;
    });
  }

  CounterController _counterController = CounterController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // _counterController.addCounter.add(snapshot.data! + 1);
          },
          child: Icon(Icons.add),
          elevation: 0,
        ),
        body: StreamBuilder(
          stream: Connectivity().onConnectivityChanged,
          builder: (BuildContext context, snapshot) {
            if (!snapshot.hasData) {
              return Text("NO DATA");
            } else {
              return Center(
                child: Column(
                  children: [
                    Text(
                      snapshot.data!.name,
                      style: TextStyle(fontSize: 66),
                    ),
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
